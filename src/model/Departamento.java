/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author Tomas
 */
public class Departamento implements Serializable {

    private String coddep;
    private String nombre;
    private Date fecha;
    private int horas;
    private Set<Asignatura> listaAsignaturas;
    private Aula aula;

    public Departamento(String coddep, String nombre, Date fecha, int horas, Aula a) {
        this.coddep = coddep;
        this.nombre = nombre;
        this.fecha = fecha;
        this.horas = horas;
        this.aula = a;
    }

    public Departamento() {
    }

    public String getCoddep() {
        return coddep;
    }

    public void setCoddep(String coddep) {
        this.coddep = coddep;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public Set<Asignatura> getListaAsignaturas() {
        return listaAsignaturas;
    }

    public void setListaAsignaturas(Set<Asignatura> listaAsignaturas) {
        this.listaAsignaturas = listaAsignaturas;
    }

    public Aula getAula() {
        return aula;
    }

    public void setAula(Aula aula) {
        this.aula = aula;
    }

    @Override
    public String toString() {
        return "Departamento{" + "coddep=" + coddep + ", nombre=" + nombre + ", fecha=" + fecha + ", horas=" + horas + ", listaAsignaturas=" + listaAsignaturas + ", aula=" + aula + '}';
    }

}
