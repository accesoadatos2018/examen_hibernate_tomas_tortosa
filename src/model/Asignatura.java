/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author Tomas
 */
public class Asignatura implements Serializable {

    private int codas;
    private Nombre nombre;
    private Tipo tipo;
    private Departamento departamento;

    public Asignatura(Nombre nom, Tipo tipo, Departamento d) {
        this.nombre = nom;
        this.tipo = tipo;
        this.departamento = d;
    }

    public Asignatura() {
    }

    public int getCodas() {
        return codas;
    }

    public void setCodas(int codas) {
        this.codas = codas;
    }

    public Nombre getNombre() {
        return nombre;
    }

    public void setNombre(Nombre nombre) {
        this.nombre = nombre;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @Override
    public String toString() {
        return "Asignatura{" + "codas=" + codas + ", nombre=" + nombre + ", tipo=" + tipo + ", departamento=" + departamento + '}';
    }

}
