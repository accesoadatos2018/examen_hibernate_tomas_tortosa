/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import model.Asignatura;
import model.Aula;
import model.Departamento;
import model.Nombre;
import model.Tipo;
import model.profesor;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Maite
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //CREAMOS CONEXION
        int opcion = 0;
        Scanner teclado = new Scanner(System.in);

        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        configuration.configure();
        sessionFactory = configuration.buildSessionFactory();
        SessionFactory factory = new Configuration().configure().buildSessionFactory();

        //CREAR UNA SESION
        Session session = factory.openSession();
        session.beginTransaction();
        Departamento d = null;

        do {

            menu();
            opcion = teclado.nextInt();

            switch (opcion) {

                case 0: //SALIR
                    
                    System.out.println("Saliendo...");
                    
                    break;
                
                case 1: // INSERTAR BD

                    Set<Asignatura> listaAsignaturas = new HashSet<>();

                    Calendar c = Calendar.getInstance();
                    c.set(2019, 04, 10);

                    // CREAMOS UN OBJETO
                    //profesor profesor=new profesor(7,"Pepe","Garcia","Perez");
                    Nombre nom = new Nombre("Tecnologia1");

                    Aula a = new Aula("5", "Tecnologia");
                    d = new Departamento("Tec", "tecnologia", c.getTime(), 10, a);

                    Asignatura a1 = new Asignatura(nom, Tipo.Trimestral, d);
                    listaAsignaturas.add(a1);

                    d.setListaAsignaturas(listaAsignaturas);

                    //GUARDAR OBJETO
                    session.save(d);
                    session.getTransaction().commit();

                    break;
                    
                case 2: // MODIFICAR
                    session.beginTransaction();
                    
                    d.setNombre("Tecnologia");
                    
                    session.update(d);
                    session.getTransaction().commit();
                    
                    break;
                    
                case 3: //ELIMINAR
                    session.beginTransaction();
                    session.delete(d);
                    session.getTransaction().commit();
                    
                    break;

                case 4: // CONSULTA

                    Query query = session.createQuery("SELECT a.nombre FROM Asignatura a WHERE a.nombre LIKE 'C%' OR a.nombre LIKE 'X%'");
                    List<Nombre> llistaNoms = query.list();

                    System.out.println("Nombre Asignaturas nombre empieza por C o por X");

                    for(Nombre n : llistaNoms){
                        
                        System.out.println(n);
                        
                    }
                    
                    break;
                    
                case 5: // CONSULTA
                    
                    query = session.createQuery("SELECT a.nombre FROM Asignatura a WHERE a.tipo = (SELECT a.tipo FROM Asignatura a WHERE codas=9)");
                    llistaNoms = query.list();
                    
                    System.out.println("Nombre Asignaturas que son del mismo tipo que la Asignatura con codigo 9");
                    
                    for(Nombre n : llistaNoms){
                        
                        System.out.println(n);
                        
                    }
                    
                    break;
                    
                case 6: // CONSULTA
                    
                    query = session.createQuery("SELECT d.coddep, d.nombre, d.fecha, d.horas FROM Departamento d WHERE d.horas > (SELECT AVG(d.horas) FROM Departamento d)");
                    List<Object[]> listaDepartamentos = query.list();
                    
                    for(Object[] o : listaDepartamentos){
                        
                        System.out.println("Coddep: " + o[0] + "\n" +
                                " Nombre: " + o[1] + "\n" +
                                " Fecha: " + o[2] +  "\n" + 
                                " Horas: " + o[3]);
                        
                    }
                    
                    break;
                    
                default:
                    throw new AssertionError();

            }

        } while (opcion != 0);

        //CERRAR CONEXION
        
        session.close();
        factory.close();

    }

    public static void menu() {

        System.out.println("Selecciona opción");
        System.out.println("0. Salir");
        System.out.println("1. Insertar en BD");
        System.out.println("2. Modificar BD");
        System.out.println("3. Borrar BD");
        System.out.println("4. Consulta Asignaturas cuyo nombre empieza por C o por X");
        System.out.println("5. Consulta Nombre de las Asignaturas que sean del mismo tipo que la Asignatura con codigo 9");
        System.out.println("6. Seleccionar Departamento que tienen más horas que la media");

    }

}
